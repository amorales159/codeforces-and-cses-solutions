#include <iostream>
#include <vector>
#include <math.h>
#include <numeric>

using namespace std;

// https://codeforces.com/contest/165/problem/B/

int n, k;
vector<long long> powers;

bool can_reach_n_lines (int v) {
    long total = 0;
    for (int i = 0; i < powers.size(); i++) {
        long curr_val = v / powers[i];
        total += curr_val;
        if (!curr_val)
            return false;
        if (total >= n)
            return true;
    }
    return false;
}

int binary_search (int low, int high) {
    // condition can't be met
    int mid;
    while (true) {
        if (high - low == 1) {
            return high;
        }
        mid = ((high - low) / 2) + low;
        // check if can meet n lines with mid v
        if (can_reach_n_lines(mid))
            high = mid;
        else
            low = mid;
    }
    return 0;
}

int main () {
    cin >> n >> k;
    powers.resize(ceil(log(n) / log(k)) + 1);
    powers[0] = 1;
    // precompute power values of k
    for (int i = 1; i < powers.size(); i++) {
        powers[i] = powers[i - 1] * k;
    }
    cout << binary_search(0, n) << endl;
}