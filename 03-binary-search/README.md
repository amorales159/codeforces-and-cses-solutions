## Binary Search

The following problems are associated with binary search and are as follows:
* [Burning Midnight Oil](https://codeforces.com/contest/165/problem/B/) [Codeforces]&emsp;(burning.cpp)
* [Apartments](https://www.cses.fi/problemset/task/1084/) [CSES]&emsp;(apartments.cpp)