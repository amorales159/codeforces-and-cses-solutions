#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// https://www.cses.fi/problemset/task/1084/

int n, m, k;

vector<int> desired_sizes;
vector<int> desired_size_bounds;
vector<int> apartment_sizes;

int get_max () {
    // sort, use two pointers (one on each arr), start w/ requests. Move over
    // apartments if any satisfy range, move up if not. If out of range, advance
    // to next applicant (desired_size_bounds = 2n)
    int num_matches = 0;
    long applicant_i = 0;
    long apartment_i = 0;
    while (applicant_i < desired_size_bounds.size() && apartment_i < apartment_sizes.size()) {
        int lower_bound = desired_size_bounds[applicant_i];
        int upper_bound = desired_size_bounds[applicant_i + 1];
        // iterate over apartments if one can satisfy
        while (apartment_i < apartment_sizes.size() && apartment_sizes[apartment_i] < lower_bound) {
            // increment apartments (less than lower bound)
            ++apartment_i;
        }
        if (apartment_i < apartment_sizes.size() && apartment_sizes[apartment_i] <= apartment_sizes[apartment_i] && apartment_sizes[apartment_i] <= upper_bound) {
            // found eligible apartment 
            ++num_matches;
            ++apartment_i;
        }
        applicant_i += 2;
    }
    return num_matches;
}

int main () {
    cin >> n >> m >> k;
    for (int i = 0; i < n; i++) {
        int a;
        cin >> a;
        // push both lower and upper bound, vector will have size 2n
        desired_sizes.push_back(a);
    }
    for (int i = 0; i < m; i++) {
        int b;
        cin >> b;
        apartment_sizes.push_back(b);
    }
    // sort asc
    sort(desired_sizes.begin(), desired_sizes.end());
    sort(apartment_sizes.begin(), apartment_sizes.end());
    // fill with vector with desired bounds: lower and upper bound
    for (int i = 0; i < desired_sizes.size(); i++) {
        desired_size_bounds.push_back(desired_sizes[i] - k);
        desired_size_bounds.push_back(desired_sizes[i] + k);
    }
    cout << get_max() << endl;
}