#include <iostream>
#include <cmath>
#include <tuple>
#include <algorithm>
#include <vector>

using namespace std;

// https://www.cses.fi/problemset/task/1197/

int main () {
    int n, m;
    cin >> n >> m;
    vector<double> dist(n, 0);
    // {a, b, c}
    vector<tuple<int, int, int>> edges;
    vector<int> parents(n, 0);

    // input edges
    for (int i = 0; i < m; i++) {
        int a, b, c;
        cin >> a >> b >> c;
        edges.push_back(make_tuple(a - 1, b - 1, c));
    }

    dist[get<0>(edges[0])] = 0;

    int cycle_node = 0;

    // bellman ford (store parents of nodes)
    for (int i = 0; i < n; i++) {
        cycle_node = -1;
        for (int e = 0; e < int(edges.size()); e++) {
            int a = get<0>(edges[e]);
            int b = get<1>(edges[e]);
            int c = get<2>(edges[e]);
            if (dist[b] > dist[a] + c) {
                dist[b] = dist[a] + c;
                parents[b] = a;
                cycle_node = b;
            }
        }
    }

    if (cycle_node == -1) {
        cout << "NO" << endl;
        return 0;
    }

    cout << "YES" << endl;

    // trace back
    for (int i = 0; i < n; i++) {
        cycle_node = parents[cycle_node];
    }

    vector<int> neg_cycle_path;

    // record negative cycle
    for (int curr_node = cycle_node; ; curr_node = parents[curr_node]) {
        neg_cycle_path.push_back(curr_node);
        if (curr_node == cycle_node && neg_cycle_path.size() > 1) {
            break;
        }
    }

    // fix direction
    reverse(neg_cycle_path.begin(), neg_cycle_path.end());

    for (int i = 0; i < int(neg_cycle_path.size()); i++) {
        cout << neg_cycle_path[i] + 1 << " ";
    }
    cout << endl;
}