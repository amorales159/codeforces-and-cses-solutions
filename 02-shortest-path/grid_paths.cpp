#include <iostream>
#include <string>
#include <vector>
 
using namespace std;

// https://www.cses.fi/problemset/task/1625/
 
int visited[9][9] = {};
string path;
int num_paths = 0;
 
void take_path(int i, int r, int c) {
    // if grid is split vertically with pos at edge, no way to complete path
    bool split_vertically = ((c >= 2 && c <= 6 && !visited[r][c + 1] && !visited[r][c - 1]) && 
    ((r == 0 && visited[r + 1][c]) || (r == 7 && visited[r - 1][c])));

    // if grid is split horizontally with pos at edge, no way to complete path
    bool split_horizontally = ((r >= 2 && r <= 6 && !visited[r + 1][c] && !visited[r - 1][c]) &&
    ((c == 0 && visited[r][c + 1]) || (c == 7 && visited[r][c - 1])));

    bool visited_split = (r >= 2 && r <= 5 && c >= 2 && c <= 6 && visited[r + 1][c]
    && visited[r - 1][c] && !visited[r][c + 1] && !visited[r][c - 1]);

    bool visited_split2 = (r >= 2 && r <= 5 && c >= 2 && c <= 6 && visited[r][c + 1]
    && visited[r][c - 1] && !visited[r + 1][c] && !visited[r - 1][c]);

    if (split_vertically || split_horizontally || visited_split || visited_split2) {
        return;
    }

    if ((visited[r][c - 1] && visited[r][c + 1]) && (!visited[r - 1][c] && !visited[r + 1][c])) {
        return;
    } else if ((visited[r - 1][c] && visited[r + 1][c]) && (!visited[r][c - 1] && !visited[r][c + 1])) {
        return;
    }
 
    char dir = path[i + 1];
    visited[r][c] = 1;
 
    // check if path found (if not at last turn, invalid path)
    if (r == 1 && c == 7) {
        if (i == 47) {
            ++num_paths;
        }
        visited[r][c] = 0;
        return;
    }
 
    // move up
    if (dir == 'U' || dir == '?') {
        if (!visited[r][c - 1]) {
            take_path(i + 1, r, c - 1);
        }
    }
 
    // move right
    if (dir == 'R' || dir == '?') {
        if (!visited[r + 1][c]) {
            take_path(i + 1, r + 1, c);
        }
    }
 
    // move down
    if (dir == 'D' || dir == '?') {
        if (!visited[r][c + 1]) {
            take_path(i + 1, r, c + 1);
        }
    }
 
    // move left
    if (dir == 'L' || dir == '?') {
        if (!visited[r - 1][c]) {
            take_path(i + 1, r - 1, c);
        }
    }
 
    visited[r][c] = 0;
    return;
}
 
int main () {
    getline(cin, path);

    // set boundary
    for (int i = 0; i < 9; i++) {
        visited[i][8] = 1;
        visited[8][i] = 1;
        visited[0][i] = 1;
        visited[i][0] = 1;
    }

    take_path(-1, 1, 1);
    cout << num_paths << endl;
    return 0;
}