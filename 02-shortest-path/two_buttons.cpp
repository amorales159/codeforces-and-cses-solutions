#include <iostream>
#include <chrono>
#include <queue>

using namespace std;

// https://codeforces.com/contest/520/problem/B/

int get_presses () {
    long n, m;
    cin >> n >> m;
    int times_pressed = 0;

    while (n != m) {
        if (m > n && !(m % 2)) {
            // if m is greater than n and is even, divide by 2
            m /= 2;
            ++times_pressed;
        } else {
            // otherwise, increment m
            ++m;
            ++times_pressed;
        } 
    }
    return times_pressed;
}

int main () {
    cout << get_presses() << endl;
}