#include <iostream>
#include <vector>
#include <stack>

using namespace std;

// https://codeforces.com/contest/320/problem/B/

bool dfs(int a_i, int b_i, vector<int>& visited, const vector<pair<int, int>>& intervals) {
    stack<int> st;
    st.push(a_i);

    while (!st.empty()) {
        int current = st.top();
        st.pop();

        // visited, skip
        if (visited[current]) {
            continue;
        }

        visited[current] = true;

        const pair<int, int>& a = intervals[current];
        const pair<int, int>& b = intervals[b_i];

        // check if completed path
        if (current == b_i || (b.first < a.first && a.first < b.second) || (b.first < a.second && a.second < b.second)) {
            return true;
        }

        // iterate over potential intervals, push if connected
        for (int new_i = 0; new_i < visited.size(); new_i++) {
            if (!visited[new_i] && ((intervals[new_i].first < a.first && a.first < intervals[new_i].second) || 
                                    (intervals[new_i].first < a.second && a.second < intervals[new_i].second))) {
                st.push(new_i);
            }
        }
    }

    // no path found
    return false;
}

int main () {
    int n;
    cin >> n;
    vector<pair<int, int>> intervals;
    vector<int> visited;

    for (int i = 0; i < n; i++) {
        int type;
        cin >> type;
        if (type == 1) {
            // add new interval
            int x, y;
            cin >> x >> y;
            intervals.push_back(make_pair(x, y));
            visited.push_back(0);
        } else {
            // check interval
            int a, b;
            cin >> a >> b;
            fill(visited.begin(), visited.end(), 0);
            bool path = dfs(a - 1, b - 1, visited, intervals);

            if (path)
                cout << "YES" << endl;
            else
                cout << "NO" << endl;
        }
    }
    return 0;
}
