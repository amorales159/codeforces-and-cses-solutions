## Greedy Algorithms

The following problems are associated with greedy algorithms and are as follows:
* [Before an Exam](https://codeforces.com/contest/4/problem/B/) [Codeforces]&emsp;(exam.cpp)
* [Stick Lengths](https://www.cses.fi/problemset/task/1074/) [CSES]&emsp;(sticks.cpp)