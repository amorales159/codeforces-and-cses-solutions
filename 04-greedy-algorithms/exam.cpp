#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// https://codeforces.com/contest/4/problem/B/

bool can_schedule (vector<int> &final_schedule, int d, int time_needed) {
    vector<pair<int, int>> times;
    
    int total_max = 0;
    int total_min = 0;
    for (int i = 0; i < d; i++) {
        int min, max;
        cin >> min >> max;
        total_min += min;
        total_max += max;
        final_schedule[i] = min;
        times.push_back(make_pair(min, max));
    }
    int remaining = time_needed - total_min;
    // minimum hours exceed the time needed
    if (remaining < 0)
        return false;
    // find schedule solution
    for (int i = 0; i < d; i++) {
        int cur_max = times[i].second;
        // if additional time can be added to the current min at the schedule i, add it
        int additional_time = min(remaining, cur_max - final_schedule[i]);
        final_schedule[i] += additional_time;
        remaining -= additional_time;
    }
    // if there is any remaining hours that couldn't be added, schedule not possible
    if (remaining > 0)
        return false;
    else
        return true;
}

int main () {
    int d, time_needed;
    cin >> d >> time_needed;
    vector<int> final_schedule(d);
    if (!can_schedule(final_schedule, d, time_needed)) {
        cout << "NO";
    } else {
        cout << "YES" << endl;
        for (int i = 0; i < d; i++) {
            cout << final_schedule[i] << " ";
        }
    }
    cout << endl;
}