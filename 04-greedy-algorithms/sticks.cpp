#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// https://www.cses.fi/problemset/task/1074/

int main () {
    int n;
    vector<long> p;
    cin >> n;
    for (int i = 0; i < n; i++) {
        long curr_length;
        cin >> curr_length;
        p.push_back(curr_length);
    }
    sort(p.begin(), p.end());
    int mid = p[ p.size() / 2 ];
    long total_diff = 0;
    for (int i = 0; i < p.size(); i++) {
        total_diff += abs(p[i] - mid);
    }
    cout << total_diff << endl;
}