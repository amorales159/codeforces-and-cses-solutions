#include <iostream>
#include <vector>

// https://codeforces.com/contest/277/problem/A

using namespace std;

vector<int> parents(101);
vector<int> frequency(101, 0);
vector<int> ranks(101, 0);

int zero_lang = 0;
int total = 0;

int find (int child) {
    if (child != parents[child]) {
        // path compression
        parents[child] = find(parents[child]);
    }
    return parents[child];
}

void perform_union (int x, int y) {
    int x_root = find(x);
    int y_root = find(y);
    if (x_root == y_root) {
        return;
    }
    if (ranks[x_root] < ranks[y_root]) {
        parents[x_root] = parents[y_root];
    } else {
        parents[y_root] = x_root;
        if (ranks[x_root] == ranks[y_root]) {
            ++ranks[y_root];
        }
    }
}

int main () {
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < 101; i++) {
        parents[i] = i;
    }
    int k, l , r;
    for (int i = 0; i < n; i++) {
        cin >> k;
        if (k == 0) {
            ++zero_lang;
            continue;
        }
        cin >> l;
        ++frequency[l];
        for (int j = 1; j < k; j++) {
            cin >> r;
            ++frequency[r];
            perform_union(l, r);
        }
    }
    int min_spent = 0;
    for (int i = 1; i <= m; i++) {
        if (!frequency[i]) {
            ++total;
        } else if (parents[i] == i) {
            ++min_spent;
        }
    }
    --min_spent;
    if (total == m) {
        cout << n << endl;
    } else {
        cout << min_spent + zero_lang << endl;
    }
}