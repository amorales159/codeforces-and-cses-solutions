#include <iostream>
#include <vector>

using namespace std;
 
 // https://www.cses.fi/problemset/task/1676/

vector<pair<int, long>> parents;

int num_comp;
long largest_comp = 1;

int find (int child) {
    if (parents[child].first != child) {
        // path compression
        parents[child].first = find(parents[child].first);
    }
    return parents[child].first;
}

void perform_union (int x, int y) {
    int x_root = find(x);
    int y_root = find(y);
    if (x_root != y_root) {
        --num_comp;
        int new_size = parents[x_root].second + parents[y_root].second;
        // update the size of the new component in the root nodes
        parents[y_root].second = new_size;
        parents[x_root].second = new_size;
        if (new_size > largest_comp) {
            largest_comp = new_size;
        }
        parents[y_root].first = x_root;
    }
}

int main () {
    int n, m;
    cin >> n >> m;
    num_comp = n;
    for (int i = 0; i < n; i++) {
        // store pair of node index, and size of that component
        parents.push_back({i, 1});
    }
    for (int i = 0; i < m; i++) {
        int a, b;
        cin >> a >> b;
        perform_union(a - 1, b - 1);
        cout << num_comp << " " << largest_comp << endl;
    }
}