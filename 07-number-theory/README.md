## Number Theory

The following problems are associated with number theory and are as follows:
* [Design Tutorial: Learn from Math](https://codeforces.com/contest/472/problem/A/) [Codeforces]&emsp;(learn.cpp)
* [Exponentation II](https://www.cses.fi/problemset/task/1712/) [CSES]&emsp;(exponent.cpp)