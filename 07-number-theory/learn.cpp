#include <iostream>

using namespace std;

// https://codeforces.com/contest/472/problem/A/

int main () {
    int n;
    cin >> n;
    // even (any even number >12 - 4 is composite)
    if (!(n % 2))
        cout << "4 " << n - 4 << endl;
    // odd (any odd number >12 - 9 is composite)
    else
        cout << "9 " << n - 9 << endl;
}