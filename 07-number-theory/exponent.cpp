#include <iostream>
#include <vector>
#include <algorithm>
#include <math.h>

using namespace std;

// https://www.cses.fi/problemset/task/1712/

long long calc(long long b, long long e, long long mod) {
    b %= mod;
    long long ans = 1;
    while (e > 0) {
        // mod exp
        if (e % 2)
            ans = ans * b % mod;
        b = b * b % mod;
        e /= 2;
    }
    return ans;
}

int main () {
    int n;
    cin >> n;
    for (int i = 0; i < n; i++) {
        long long a, b, c;
        cin >> a >> b >> c;
        // fermat's little theorem! ( a^(p - 1) = 1(mod p) )
        long long b_to_c = calc(b, c, 1e9 + 6);
        cout << calc(a, b_to_c, 1e9 + 7) << endl;
    }
}