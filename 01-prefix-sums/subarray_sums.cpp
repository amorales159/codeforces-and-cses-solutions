#include <iostream>
#include <vector>
#include <map>

using namespace std;

// https://www.cses.fi/problemset/task/1661/

int main () {
    int n, x;
    cin >> n >> x;
    vector<int> array(n);
    long subarrays = 0;

    // store array
    for (int i = 0; i < n; i++) {
        cin >> array[i];
    }
    
    long prefix_sum = 0;
    // hashmap to store seen prefix sums (k: prefix sum, v: times seen)
    map<long, int> seen;
    // store prefix sum 0 as seen (set value to 1)
    seen[0] = 1;

    for (int e = 0; e < n; e++) {
        // calculate prefix sum from [0, e]
        prefix_sum += array[e];
        // find difference between prefix sum [0, e] and x
        // number of counted prefix sums at index m gives how many in [0, m] subarray
        long m = prefix_sum - x;
        // if prefix sum exists, increment by how many times seen
        if (seen.find(m) != seen.end()) {
            subarrays += seen[m];
        }
        // add prefix sum to hashmap/increment value
        if (seen.find(prefix_sum) != seen.end()) {
            ++seen[prefix_sum];
        } else {
            seen[prefix_sum] = 1;
        }
    }
    cout << subarrays;
}
