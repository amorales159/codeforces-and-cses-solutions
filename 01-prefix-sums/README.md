## Prefix Sums

The following problems are associated with prefix sums / sliding window and are as follows:
* [Greg and Array](https://codeforces.com/contest/295/problem/A/) [Codeforces]&emsp;(greg.cpp)
* [Subarray Sums II](https://www.cses.fi/problemset/task/1661/) [CSES]&emsp;(subarray_sums.cpp)