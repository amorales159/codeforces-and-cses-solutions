#include <iostream>
#include <vector>

using namespace std;

// https://codeforces.com/contest/295/problem/A/

int main () {
    int n, m, k;
    cin >> n >> m >> k;
    vector<long long> array(n + 2, 0);
    vector<long long> values(m + 1, 0);

    // get starting array
    for (int i = 1; i <= n; i++) {
        cin >> array[i];
    }

    // store all operations
    vector<vector<int>> operations(m + 2);
    for (int i = 1; i <= m; i++) {
        int l, r, d;
        cin >> l >> r >> d;
        operations[i].push_back(l);
        operations[i].push_back(r);
        values[i] = d;
    }

    // count each operation
    vector<int> operation_count(m + 2, 0);
    for (int i = 1; i <= k; i++) {
        int x, y;
        cin >> x >> y;
        operation_count[x]++;
        operation_count[y + 1]--;
    }

    // prefix sum on operation count
    for (int i = 1; i <= m; i++) {
        operation_count[i] += operation_count[i - 1];
    }

    // perform operations
    vector<long long> performed(n + 2, 0);
    for (int i = 1; i <= m; i++) {
        if (operation_count[i]) {
            int l = operations[i][0];
            int r = operations[i][1];
            performed[l] += static_cast<long long>(operation_count[i]) * values[i];
            performed[r + 1] -= static_cast<long long>(operation_count[i]) * values[i];
        }
    }

    // prefix sum on performed operations
    // update array
    for (int i = 1; i <= n; i++) {
        performed[i] += performed[i - 1];
        array[i] += performed[i];
    }

    // print array
    for (int a = 1; a < n; a++) {
        cout << array[a] << " ";
    }
    cout << array[n];
}