#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// https://codeforces.com/contest/189/problem/A/

int n, a, b, c;
vector<int> M;
vector<int> cutting;

int cut_ribbon (int length) {
    for (int i = 1; i <= n; i++) {
        for (int j = 0; j < 3; j++) {
            if (i >= cutting[j] && M[i - cutting[j]] != -1) {
                M[i] = max(M[i], 1 + M[i - cutting[j]]);
            }
        }
    }
    return M[n];
}

int main () {
    cin >> n;
    for (int i = 0; i < 3; i++) {
        int cur;
        cin >> cur;
        cutting.push_back(cur);
    }
    M = vector<int>(n + 1, -1);
    M[0] = 0;
    cout << cut_ribbon(n);
}