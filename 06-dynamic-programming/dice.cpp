#include <iostream>
#include <cstring>
#include <vector>

using namespace std;

// https://www.cses.fi/problemset/task/1633/

int n;
vector<long long> M;

int num_ways (int remaining_blocks) {
    if (M[remaining_blocks] != 0)
        return M[remaining_blocks];
    for (int i = 1; i <= 6; i++) {
        if (remaining_blocks - i >= 0) {
            M[remaining_blocks] += num_ways(remaining_blocks - i);
            M[remaining_blocks] %= 1000000007;
        }
    }
    return M[remaining_blocks];
}

int main () {
    int n;
    cin >> n;
    M = vector<long long>(1000001, 0);
    M[0] = 1;
    cout << num_ways(n);
}