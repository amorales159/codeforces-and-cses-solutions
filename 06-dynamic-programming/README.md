## Dynamic Programming

The following problems are associated with dynamic programming and are as follows:
* [Cut Ribbon](https://codeforces.com/contest/189/problem/A/) [Codeforces]&emsp;(ribbon.cpp)
* [The Fair Nut and the Best Path](https://codeforces.com/contest/1083/problem/A/)&emsp;(fair_nut.cpp)
* [Dice Combinations](https://www.cses.fi/problemset/task/1633/) [CSES]&emsp;(dice.cpp)
* [Planet Queries I](https://www.cses.fi/problemset/task/1750/) [CSES]&emsp;(planets.cpp)