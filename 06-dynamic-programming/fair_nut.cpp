#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

// https://codeforces.com/contest/1083/problem/A/

int max_n = 300005;
vector<vector<pair<int, int>>> connecting_edges(max_n);
vector<long long> weights(max_n);
vector<vector<long long>> d_p(max_n, vector<long long>(2));
long long ans;

bool is_leaf (int cur, int parent) {
    return (connecting_edges[cur].size() == 1 && connecting_edges[cur][0].first == parent);
}

void dfs (int cur, int parent) {
    vector<long long> child_weights;
    if (is_leaf(cur, parent)) {
        d_p[cur][0] = d_p[cur][1] = weights[cur];
        ans = max(ans, weights[cur]);
        return;
    }
    for (pair<int, int> val:connecting_edges[cur]) {
        int child = val.first;
        long long weight = val.second;
        if (child == parent)
            continue;
        dfs(child, cur);
        child_weights.push_back(d_p[child][0] - weight);
    }
    sort(child_weights.rbegin(), child_weights.rend());
    int num_child = child_weights.size();
    if (num_child == 1)
        d_p[cur][0] = max(weights[cur], weights[cur] + child_weights[0]);
    else {
        d_p[cur][0] = max(weights[cur], weights[cur] + child_weights[0]);
        d_p[cur][1] = max(weights[cur], weights[cur] + child_weights[0] + child_weights[1]);
    }
    ans = max(ans, max(d_p[cur][0], d_p[cur][1]));
}

int main () {
    int n;
    cin >> n;
    for (int i = 1; i <= n; i++) {
        cin >> weights[i];
    }
    if (n == 1) {
        cout << weights[1];
        return 0;
    }
    for (int i = 1; i < n; i++) {
        int u, v, w;
        cin >> u >> v >> w;
        connecting_edges[u].push_back({v, w});
        connecting_edges[v].push_back({u, w});
    }
    dfs(1, -1);
    cout << ans;
}

