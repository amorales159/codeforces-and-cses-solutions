#include <iostream>
#include <vector>
#include <cmath>

using namespace std;

// https://www.cses.fi/problemset/task/1750/

const int MAX_N = 2e5 + 5;
const int MAX_D = 30;

int up[MAX_N][MAX_D];
int powers[MAX_D];
vector<int> dest(MAX_N);

int find_parent(int x, int d) {
    for (int i = MAX_D - 1; i >= 0; i--) {
        if (d >= powers[i]) {
            x = up[x][i];
            d -= powers[i];
        }
    }
    return x;
}

int main() {
    ios_base::sync_with_stdio(false);
    cin.tie(nullptr);
    int N, Q;
    cin >> N >> Q;
    // precompute powers
    powers[0] = 1;
    for (int i = 1; i < MAX_D; i++) {
        powers[i] = powers[i - 1] << 1;
    }
    for (int i = 1; i <= N; i++) {
        cin >> dest[i];
    }
    // precompute ancestors
    for (int i = 1; i <= N; i++) {
        up[i][0] = dest[i];
    }
    for (int d = 1; d < MAX_D; d++) {
        for (int i = 1; i <= N; i++) {
            up[i][d] = up[ up[i][d - 1] ][d - 1];
        }
    }
    // calc query
    for (int q = 0; q < Q; q++) {
        int x, d;
        cin >> x >> d;
        cout << find_parent(x, d) << '\n';
    }
}
