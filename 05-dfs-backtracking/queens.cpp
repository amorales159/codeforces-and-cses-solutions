#include <iostream>
#include <vector>

using namespace std;

// https://www.cses.fi/problemset/task/1624/

vector<vector<char>> board;
int total_ways = 0;

const int N = 8;

void create_board () {
    for (int r = 0; r < N; r++) {
        vector<char> row;
        for (int c = 0; c < N; c++) {
            char cur;
            cin >> cur;
            row.push_back(cur);
        }
        board.push_back(row);
    }
}

void print_board () {
    for (int r = 0; r < N; r++) {
        for (int c = 0; c < N; c++) {
            cout << board[r][c];
        }
        cout << endl;
    }
}

bool valid_queen (int row, int col) {
    // out-of-bounds
    if (row < 0 && row >= N && col < 0 && col >= N)
        return false;
    // if reserved, invalid
    if (board[row][col] == '*') {
        return false;
    }
    // vertical
    for (int r = 0; r < N; r++) {
        if (board[r][col] == 'Q')
            return false;
    }
    // horizontal
    for (int c = 0; c < N; c++) {
        if (board[row][c] == 'Q')
            return false;
    }
    // upper-left
    int k = 1;
    while (col - k >= 0 && row - k >= 0) {
        int top_row = row - k;
        int left_col = col - k;
        if (board[top_row][left_col] == 'Q')
            return false;
        ++k;
    }
    // upper-right
    k = 1;
    while (col + k < N && row - k >= 0) {
        int top_row = row - k;
        int right_col = col + k;
        if (board[top_row][right_col] == 'Q')
            return false;
        ++k;
    }
    // lower-left
    k = 1;
    while (col - k >= 0 && row + k < N) {
        int bottom_row = row + k;
        int left_col = col - k;
        if (board[bottom_row][left_col] == 'Q')
            return false;
        ++k;
    }
    // lower-right
    k = 1;
    while (col + k < N && row + k < N) {
        int bottom_row = row + k;
        int right_col = col + k;
        if (board[bottom_row][right_col] == 'Q')
            return false;
        ++k;
    }
    return true;
}

void dfs (int r) {
    if (r == N) {
        ++total_ways;
        return;
    }
    for (int c = 0; c < N; c++) {
        if (valid_queen(r, c)) {
            board[r][c] = 'Q';
            dfs(r + 1);
            board[r][c] = '.';
        } 
    }
    return;
}

int main () {
    create_board();
    dfs(0);
    cout << total_ways << endl;
}