## DFS Backtracking

The following problems are associated with DFS backtracking and are as follows:
* [Maze](https://codeforces.com/contest/377/problem/A/) [Codeforces]&emsp;(maze.cpp)
* [Chessboard and Queens](https://www.cses.fi/problemset/task/1624/) [CSES]&emsp;(queens.cpp)