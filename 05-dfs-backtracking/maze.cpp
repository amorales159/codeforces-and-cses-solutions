#include <iostream>
#include <vector>
#include <algorithm>
#include <stack>

using namespace std;

// https://codeforces.com/contest/377/problem/A/

int ROWS;
int COLS;
int k;
bool open[501][501] = {0};
char maze[501][501] = {0};

void create_maze () {
    for (int r = 0; r < ROWS; r++) {
        for (int c = 0; c < COLS; c++) {
            char cur;
            cin >> cur;
            maze[r][c] = cur;
        }
    }
}

void print_maze () {
    for (int r = 0; r < ROWS; r++) {
        for (int c = 0; c < COLS; c++) {
            cout << maze[r][c];
        }
        cout << endl;
    }
}

void find_connectivity(int r, int c) {
    open[r][c] = 1;

    if (!open[r - 1][c] && maze[r - 1][c] == '.') find_connectivity(r - 1, c);
    if (!open[r][c + 1] && maze[r][c + 1] == '.') find_connectivity(r, c + 1);
    if (!open[r + 1][c] && maze[r + 1][c] == '.') find_connectivity(r + 1, c);
    if (!open[r][c - 1] && maze[r][c - 1] == '.') find_connectivity(r, c - 1);

    if (k > 0) {
        maze[r][c] = 'X';
        --k;
    }
}

int main () {
    int n, m;
    cin >> n >> m >> k;
    ROWS = n;
    COLS = m;
    create_maze();
    for (int i = 0; i < ROWS; i++) {
        for (int j = 0; j < COLS; j++) {
            if (maze[i][j] == '.') {
                find_connectivity(i, j);
                print_maze();
                return 0;
            }
        }
    }
}