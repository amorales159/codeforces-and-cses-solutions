## Introduction

The following problems are associated with an introduction to competitive programming and are as follows:
* [Gravity Flip](https://codeforces.com/contest/405/problem/A/) [Codeforces]&emsp;(gravity-flip.cpp)
* [Weird Algorithm](https://www.cses.fi/problemset/task/1068/) [CSES]&emsp;(weird-algorithm.cpp)