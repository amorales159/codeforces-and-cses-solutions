#include <iostream>
#include <vector>
#include <algorithm>
#include <string> // stoi
using namespace std;

// https://codeforces.com/contest/405/problem/A/

int main () {
    int col = 0;
    cin >> col;
    string curr_col;
    vector<int> box;
    for (int c = 0; c < col; c++) {
        cin >> curr_col;
        box.push_back(stoi(curr_col));
    }
    sort(box.begin(), box.end());
    for (int i = 0; i < box.size() - 1; i++) {
        cout << box[i] << " ";
    }
    cout << box[col - 1];
}