#include <iostream>

using namespace std;

// https://www.cses.fi/problemset/task/1068/

int main() {
    unsigned long n = 0;
    cin >> n;
    while (n != 1) {
        cout << n << " ";
        if (n % 2) {
            n = n * 3 + 1;
        } else {
            n /= 2;
        }
    }
    cout << 1;
}